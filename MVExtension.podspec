Pod::Spec.new do |spec|

  spec.name         = "MVExtension"
  spec.version      = "0.1.7"
  spec.summary      = "Collection of Extensions on swift"
  spec.description  = "This is a Collection of Extensions on swift"
  spec.homepage     = "https://bitbucket.org/sealcn/mvextension/src/master/"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "sunny" => "sunyang@dailyinnovation.biz" }

  spec.swift_version= "5.0"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  spec.platform     = :ios, "10.0"  

  spec.source       = { :git => "git@bitbucket.org:sealcn/mvextension.git", :tag => spec.version }

  # spec.source_files  = "Source", "Source/*/*.swift"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  # 是否支持arc
  spec.requires_arc = true

  # spec.default_subspec = ["Foundation"]

  spec.subspec 'Foundation' do |ss|
    ss.source_files = 'Source/Foundation/*.swift'
    ss.weak_frameworks = 'CryptoKit'
  end
  
  spec.subspec 'UIKitExtension' do |ss|
      ss.source_files = 'Source/UIKit/*.swift'
      ss.dependency 'MVExtension/Foundation'
  end

  spec.subspec 'Then' do |ss|
    ss.source_files = 'Source/Then/*.swift'
  end
  
  

end
