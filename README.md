# MVExtension



### Install

```

```







## String

### Localized

**Swift:**

```swift
// MARK: String.Localized
"something".localized // something
"something * %s".localized(with: "5") // something * 5
"something * %1s * %2s".localized(with: ["%1s": "100", "%2s": "200"]) //something * 100 * 200
```

**ObjC**

```objective-c
@"something".localized; // something * 5
[@"something %s" localizedWithArgs:@[@"5"]]; // something * 5
[@"something %1s, %2s" localizedWithMap:@{@"%1s": @"100", @"%2s": @"200"}]; //something * 100 * 200
```



### URLEncode

**Swift:**

```swift
let url = "http://www.baidu.com/你好"
let encodeUrl = url.urlEncoded()       // http://www.baidu.com/%E4%BD%A0%E5%A5%BD
let decodeUrl = encodeUrl.urlDecoded() // http://www.baidu.com/你好
```



**ObjC**

```objc
	
```



### Encrypt

**Swift:**

```swift
"something".md5    // 437b930db84b8079c2dd804a71936b5f
"something".base64 // c29tZXRoaW5n
"something".sha    // 1af17e73721dbe0c40011b82ed4bb1a7dbe3ce29
"something".sha224 // 3ea5e0d9d5dc6d8abf5c41bd312adbaa73ee36423bf85e503a9bfd52
"something".sha256 // 3fc9b689459d738f8c88a3a48aa9e33542016b7a4052e001aaa536fca74813cb
"something".sha384 // 3ae3c5fc342497c274c50f11609eecc460328cf0b6027d865bf205955c81e4c646544eb9630ba0aaa42753bbf5b8d20a
"something".sha512 // 983d43ddff6da90f6a5d3b6172446a1ffe228b803fe64fdd5dcfab5646078a896851fe82f623c9d6e5654b3d2f363a04ec17cfb62b607437a9c7c132d511e522
```

**ObjC**

```objc
	
```


### subscript

**Swift:**

```swift
for i in 0...10 {
	  print("something"[i])
}
```

```
s
o
m
e
t
h
i
n
g
Fatal error: String index is out of bounds: file Swift/StringRangeReplaceableCollection.swift, line 302
```



## URL



### StringLiteral

对 URL 实现 ExpressibleByStringLiteral 协议，可以通过字符串创建 URL 类型的对象。

**Swift：**

```swift
let url: URL = "http://www.baidu.com"
        
func someMethod(url: URL) {
	print(url.absoluteString)
}
        
someMethod(url: "http://www.baidu.com")
```



### MD5

获取本地文件的 md5 值

**Swift：**

```swift
let url: URL = "some local file url"
url.md5
```



**Objc：**

```objc

```



## Data



对 Data 类型提供一些加密的方法

**Swift**

```swift
let data = Data()
data.md5
data.sha1
data.sha224
data.sha256
data.sha384
data.sha512
```





## Date







## UserDefault

### subscript

**Swift**

```swift
UserDefaults.standard["key"] = "value"
let val = UserDefaults.standard["key"]
```







## UIColor

```swift
convenience init(hex: Int, alpha: CGFloat = 1.0)
```

```swift
convenience init(hexString str: String, alpha: CGFloat = 1.0)
```

```swift
class var random: UIColor
```

```swift
var toGray: UIColor
```

```swift
@objc var bright: CGFloat
```

```swift
@objc var uintOrder32LittleValue: UInt32
```





## UIImage

```swift
@objc func unsafeBitmapData() -> UnsafeMutablePointer<UInt32>?
```

```swift
@objc static func image(
        with unsafeBitmapData: UnsafeMutablePointer<UInt32>,
        canvasWidth: Int,
        canvasHeight: Int,
        autoFree: Bool = false)
    -> UIImage?
```

```swift
 convenience init(color: UIColor, size: CGSize)
```

```swift
 @objc static func create(with color: UIColor, size: CGSize) -> UIImage?
```



**TODO：** PDF support





## UITableView 



```swift
@objc var indexPathForLastRow: IndexPath?
```



```swift
var lastSection: Int?
```



```swift
@objc func indexPathForLastRow(inSection section: Int) -> IndexPath?
```



```swift
@objc func removeTableHeaderView() 
```



```swift
@objc func removeTableFooterView()
```



```swift
@objc func isValidIndexPath(_ indexPath: IndexPath) -> Bool
```



```swift
@objc func safeScrollToRow(at indexPath: IndexPath, at scrollPosition: UITableView.ScrollPosition, animated: Bool)
```





## UICollectionView



```swift
@objc var indexPathForLastRow: IndexPath?
```



```swift
var lastSection: Int?
```



```swift
@objc func indexPathForLastRow(inSection section: Int) -> IndexPath?
```



```swift
@objc func isValidIndexPath(_ indexPath: IndexPath) -> Bool
```



```swift
@objc func safeScrollToRow(at indexPath: IndexPath, at scrollPosition: UITableView.ScrollPosition, animated: Bool)
```

