//
//  AppDelegate.m
//  Example-OC
//
//  Created by mac on 2021/2/1.
//

#import "AppDelegate.h"
#import <MVExtension/MVExtension-Swift.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    
    NSLog(@"%@", [@"something" localized]);
    NSLog(@"%@", [@"something %s" localizedWithArgs:@[@"1"]]);
    NSLog(@"%@", [@"something %1s, %2s" localizedWithMap:@{@"%1s": @"100", @"%2s": @"200"}]);
    NSLog(@"%@", Device.name);

    NSString *url = @"http://www.baidu.com/你好";
    NSString *url1 = [url urlEncoded];
    NSLog(@"%@", url1);
    NSString *url2 = [url1 urlDecoded];
    NSLog(@"%@", url2);

    [UIColor hex:0x1111 alpha:1.0];
    [UIColor hex:0x111];
    
    [UIColor random];
    UIColor *color = [UIColor redColor];
    color.toGray;
    color.uintOrder32LittleValue;
    color.bright;
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
