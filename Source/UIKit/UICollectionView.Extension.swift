//
//  UICollectionView.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/22.
//

import UIKit

public extension UICollectionView {
    
    /// MVExtension: CollectionView 的最后一个 IndexPath
    var indexPathForLastItem: IndexPath? {
        return indexPathForLastItem(inSection: lastSection)
    }

    /// MVExtension: CollectionView 的最后一个 Section
    @objc var lastSection: Int {
        return numberOfSections > 0 ? numberOfSections - 1 : 0
    }
}

public extension UICollectionView {
    
    /// MVExtension: Collection 某个 section 的最后一个 IndexPath
    /// - Parameter section: index
    /// - Returns: IndexPath 或者是空
    @objc func indexPathForLastItem(inSection section: Int) -> IndexPath? {
        guard section >= 0 else {
            return nil
        }
        guard section < numberOfSections else {
            return nil
        }
        guard numberOfItems(inSection: section) > 0 else {
            return IndexPath(item: 0, section: section)
        }
        return IndexPath(item: numberOfItems(inSection: section) - 1, section: section)
    }
    
    
    /// MVExtension: 判断某个 IndexPath 是否在 CollectionView 中存在
    /// - Parameter indexPath: IndexPath
    /// - Returns: bool
    @objc func isValidIndexPath(_ indexPath: IndexPath) -> Bool {
        return indexPath.section >= 0 &&
            indexPath.item >= 0 &&
            indexPath.section < numberOfSections &&
            indexPath.item < numberOfItems(inSection: indexPath.section)
    }
    
    
    /// MVExtension: 安全的滑动到 Collection 的某个位置
    /// - Parameters:
    ///   - indexPath: indexPath
    ///   - scrollPosition: ScrollPosition
    ///   - animated: animated
    @objc func safeScrollToItem(at indexPath: IndexPath, at scrollPosition: UICollectionView.ScrollPosition, animated: Bool) {
        guard indexPath.item >= 0,
            indexPath.section >= 0,
            indexPath.section < numberOfSections,
            indexPath.item < numberOfItems(inSection: indexPath.section) else {
            return
        }
        scrollToItem(at: indexPath, at: scrollPosition, animated: animated)
    }

}
