//
//  Device.swift
//  MVExtension
//
//  Created by mac on 2021/2/2.
//

import UIKit

public class Device: NSObject {
    
    private static var shared = Device()
    
    lazy var code: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let code: String = String(validatingUTF8: NSString(bytes: &systemInfo.machine, length: Int(_SYS_NAMELEN), encoding: String.Encoding.ascii.rawValue)!.utf8String!)!
        return code
    }()
    
    lazy var isIPhoneXSeries: Bool = {
        var iPhoneXSeries = false
        if #available(iOS 11.0, *) {
            if let mainWindow = UIApplication.shared.delegate?.window {
                if mainWindow!.safeAreaInsets.bottom > 0.0 {
                    iPhoneXSeries = true
                }
            }
        }
        return iPhoneXSeries
    }()
    
    lazy var isLowEquipment: Bool = {
        let code = Device.code()
        let splitPrefix: (String) -> [String] = {
            let temp = code.replacingOccurrences(of: $0, with: "")
            return temp.components(separatedBy: ",")
        }
        if code.hasPrefix("iPhone") {
            let version = splitPrefix("iPhone")
            let big = Int(version.first ?? "100").or(100)
            return big <= 7 // iPhone 6P及以下
        } else if code.hasPrefix("iPad") {
            let version = splitPrefix("iPad")
            let big = Int(version.first ?? "100").or(100)
            return big <= 4 // iPhone 6P及以下
        } else if code.hasPrefix("iPod") {
            let version = splitPrefix("iPod")
            let big = Int(version.first ?? "100").or(100)
            return big <= 7 // iPhone 6P及以下
        }
        return false
    }()
    
    
    /// The name identifying the device (e.g. "Dennis' iPhone").
    @objc static public var name: String {
        return UIDevice.current.name
    }
    
    /// The name of the operating system running on the device represented by the receiver (e.g. "iOS" or "tvOS").
    @objc static public var systemName: String {
        return UIDevice.current.systemName
    }
    
    /// The current version of the operating system (e.g. 8.4 or 9.2).
    @objc static public var systemVersion: String {
        return UIDevice.current.systemVersion
    }
    
    /// The model of the device as a localized string.
    @objc static public var localizedModel: String {
        return UIDevice.current.localizedModel
    }
    
    @objc static func code() -> String {
        return Device.shared.code
    }
    
    /// 是否是iPhone X系列
    ///
    /// - Returns: iPhone X
    @objc public static func isIPhoneXSeries() -> Bool {
        return Device.shared.isIPhoneXSeries
    }
    
    
    /// 是否是iPhone X系列
    ///
    /// - Returns: iPhone X
    @objc public static func isiPad() -> Bool {
        var iPad = false
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            iPad = true
        }
        return iPad
    }
    
    @objc public static func isSE() -> Bool {
        if UIScreen.main.bounds.size.width < 375 {
            return true
        }
        return false
    }
    
    @objc public static func isLowEquipment() -> Bool {
        return Device.shared.isLowEquipment
    }
    
}


extension Device {
    
    public struct Application {
        
        enum Key: String {
            case FirstLaunch = "MVExtension_Device_Application_NotFirstLanuch"
        }
        
        public static let shared = Application()
        
        public private(set) var version: String
        public private(set) var build: String
        public private(set) var name: String
        public private(set) var bundleID: String
        public private(set) var wholeVersion: String
        public private(set) var firstLaunch: Bool = true
        
        fileprivate init() {
            let infoDictionary = Bundle.main.infoDictionary
            version = infoDictionary?["CFBundleShortVersionString"] as! String
            build = infoDictionary?["CFBundleVersion"] as! String
            bundleID = infoDictionary?["CFBundleIdentifier"] as! String
            if let appName = infoDictionary?["CFBundleDisplayName"] as? String {
                name = appName
            } else {
                name = infoDictionary?["CFBundleName"] as! String
            }
            wholeVersion = "\(version) (Build \(build))"
            
            let key = Key.FirstLaunch.rawValue
            let userDefaults = UserDefaults.standard
            let currentVersion = wholeVersion
            if let storeVersion = userDefaults.string(forKey: key) {
                firstLaunch = (currentVersion != storeVersion)
            }
            userDefaults.set(currentVersion, forKey: key)
            userDefaults.synchronize()
        }
    }
}
