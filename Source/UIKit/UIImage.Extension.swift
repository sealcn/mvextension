//
//  UIImage.Bitmap.swift
//  MVExtension
//
//  Created by mac on 2021/1/22.
//

import UIKit

public extension UIImage {
        
    /// MVExtension: 图片生成 bitmap
    /// - Returns: Bitmap
    @objc func unsafeBitmapData() -> UnsafeMutablePointer<UInt32>? {
        guard let cgImage = self.cgImage else { return nil }
        
        let width = Int(size.width)
        let height = Int(size.height)
        
        let bitsPerComponent = 8
        let bytesPerPixel = 4
        let bytesPerRow = width * bytesPerPixel
        let maxPix = width * height
        
        let imageData = calloc(maxPix, MemoryLayout<UInt32>.size).assumingMemoryBound(to: UInt32.self)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue
        guard let imageContext = CGContext(
            data: imageData,
            width: width, height: height,
            bitsPerComponent: bitsPerComponent,
            bytesPerRow: bytesPerRow,
            space: colorSpace,
            bitmapInfo: bitmapInfo) else { return nil }
        
        imageContext.draw(cgImage, in: CGRect(origin: CGPoint.zero, size: self.size))
        return imageData
    }
    
    
    /// MVExtension: MVExtension:通过 Bitmap 生成图片, 注意是否需要释放 Bitmap 所消耗的内存
    /// - Parameters:
    ///   - unsafeBitmapData: BitMap
    ///   - canvasWidth: 宽
    ///   - canvasHeight: 高
    ///   - autoFree: 生成后是否需要释放该 BitMap 所消耗内存
    /// - Returns:  图片
    @objc static func image(
        with unsafeBitmapData: UnsafeMutablePointer<UInt32>,
        canvasWidth: Int,
        canvasHeight: Int,
        autoFree: Bool = false)
    -> UIImage? {
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        var callback: CGDataProviderReleaseDataCallback = {_, _, _ in}
        if autoFree {
            callback = { v1, v2, _ in
                v1?.deallocate()
                v2.deallocate()
            }
        }
        let provider = CGDataProvider(dataInfo: nil, data: unsafeBitmapData, size: canvasHeight * canvasWidth * 4, releaseData: callback)
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue)

        let cgimage = CGImage(width: canvasWidth, height: canvasHeight,
                              bitsPerComponent: 8, bitsPerPixel: 32,
                              bytesPerRow: canvasWidth * 4,
                              space: colorSpace,
                              bitmapInfo: bitmapInfo,
                              provider: provider!,
                              decode: nil, shouldInterpolate: true,
                              intent: CGColorRenderingIntent.defaultIntent)
        if cgimage.hasSome {
            return UIImage(cgImage: cgimage!)
        } else {
            return nil
        }
    }
    
    
}

public extension UIImage {
    
    /// MVExtension: 通过颜色生成图片
    /// - Parameters:
    ///   - color: 颜色
    ///   - size: 图片大小
    convenience init(color: UIColor, size: CGSize) {
         UIGraphicsBeginImageContextWithOptions(size, false, 1)

         defer {
             UIGraphicsEndImageContext()
         }

         color.setFill()
         UIRectFill(CGRect(origin: .zero, size: size))

         guard let aCgImage = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else {
             self.init()
             return
         }

         self.init(cgImage: aCgImage)
     }
    
    
    /// MVExtension: 通过颜色生成图片 for oc
    /// - Parameters:
    ///   - color: 颜色
    ///   - size: 图片大小
    @objc static func create(with color: UIColor, size: CGSize) -> UIImage? {

        UIGraphicsBeginImageContext(size)
        let ctx = UIGraphicsGetCurrentContext()
        ctx?.setFillColor(color.cgColor)
        ctx?.fill(CGRect.init(origin: .zero, size: size))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
        
    }
}


public extension UIImage {
    
    // 图片生成
    static func create(with images: [UIImage?], size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        let ctx = UIGraphicsGetCurrentContext()
        
        ctx?.translateBy(x: 0, y: size.height)
        ctx?.scaleBy(x: 1.0, y: -1.0)
        
        ctx?.setFillColor(UIColor.white.cgColor)
        ctx?.fill(CGRect(origin: .zero, size: size))
        images.forEach({
            if let cgImage = $0?.cgImage {
                ctx?.draw(cgImage, in: CGRect(origin: .zero, size: size))
            }
        })

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}

public extension UIImage {
    
    /// PDF 转 UIImage
    ///
    /// - Parameters:
    ///   - url: PDF 的 URL 地址
    ///   - scale: 当前放大的倍数
    /// - Returns:  UIImage
    @objc static func imageWith(
        pdf data: Data,
        scale: CGFloat = 1)
        -> UIImage? {
           
            guard let provider = CGDataProvider(data: data as CFData) else { return nil }
            guard let document = CGPDFDocument(provider) else { return nil}
            guard let page = document.page(at: 1) else { return nil }
            
            let pageRect = page.getBoxRect(.mediaBox)
            
            let aspectRatio = pageRect.width / pageRect.height
            let w = UIScreen.main.bounds.width * scale
            let h = w / aspectRatio
            let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: w, height: h))
            
            let sizeScale = w / pageRect.width
            let renderer = UIGraphicsImageRenderer(size: rect.size)
            let img = renderer.image { ctx in
                UIColor.clear.set()
                ctx.fill(rect)
                ctx.cgContext.translateBy(x: 0.0, y: rect.size.height)
                ctx.cgContext.scaleBy(x: sizeScale, y: -sizeScale)
                ctx.cgContext.drawPDFPage(page)
            }
            return img
    }
    
    @objc static func imageWith(
        pdf data: Data,
        size: CGSize)
        -> UIImage? {
            guard let provider = CGDataProvider(data: data as CFData) else { return nil }
            guard let document = CGPDFDocument(provider) else { return nil}
            guard let page = document.page(at: 1) else { return nil }
            
            let pageRect = page.getBoxRect(.mediaBox)

            let rect = CGRect(origin: CGPoint.zero, size: size)
            
            let sizeScale = size.width / pageRect.width
            let renderer = UIGraphicsImageRenderer(size: rect.size)
            let img = renderer.image { ctx in
                UIColor.clear.set()
                ctx.fill(rect)
                ctx.cgContext.translateBy(x: 0.0, y: rect.size.height)
                ctx.cgContext.scaleBy(x: sizeScale, y: -sizeScale)
                ctx.cgContext.drawPDFPage(page)
            }
            return img
    }
}
