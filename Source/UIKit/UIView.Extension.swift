//
//  UIView.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/22.
//

import UIKit

public extension UIView {
    
    var x: CGFloat {
        get {
            return self.bounds.origin.x
        }
        set {
            var frame = self.frame
            frame.origin.x = newValue
            self.frame = frame
        }
    }
    var y: CGFloat {
        get {
            return self.bounds.origin.y
        }
        set {
            var frame = self.frame
            frame.origin.y = newValue
            self.frame = frame
        }
    }
    var width: CGFloat {
        get {
            return self.bounds.size.width
        }
        set {
            var frame = self.frame
            frame.size.width = newValue
            self.frame = frame
        }
    }
    var height: CGFloat {
        get {
            return self.bounds.size.height
        }
        set {
            var frame = self.frame
            frame.size.height = newValue
            self.frame = frame
        }
    }
    var size: CGSize {
        get {
            return self.bounds.size
        }
        set {
            var frame = self.frame
            frame.size = newValue
            self.frame = frame
        }
    }
    var centerX: CGFloat {
        get {
            return self.center.x
        }
        set {
            var center = self.center
            center.x = newValue
            self.center = center
        }
    }
    var centerY: CGFloat {
        get {
            return self.center.y
        }
        set {
            var center = self.center
            center.y = newValue
            self.center = center
        }
    }
    var top: CGFloat {
        get {
            return self.y
        }
        set {
            self.y = newValue
        }
    }
    var bottom: CGFloat {
        get {
            return self.y + self.height
        }
        set {
            self.y = newValue - self.height;
        }
    }
    var left: CGFloat {
        get {
            return self.x
        }
        set {
            self.x = newValue
        }
    }
    var right: CGFloat {
        get {
            return self.x + self.width
        }
        set {
            self.x = newValue - self.width
        }
    }
}

// MARK: - Properties

public extension UIView {
    
    
    @IBInspectable var borderColor: UIColor? {
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            // Fix React-Native conflict issue
            guard String(describing: type(of: color)) != "__NSCFType" else { return }
            layer.borderColor = color.cgColor
        }
    }
    
    /// MVExtension: Border width of view; also inspectable from Storyboard.
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    /// MVExtension: Corner radius of view; also inspectable from Storyboard.
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.masksToBounds = true
            layer.cornerRadius = abs(CGFloat(Int(newValue * 100)) / 100)
        }
    }
    
    /// MVExtension: Shadow color of view; also inspectable from Storyboard.
    @IBInspectable var layerShadowColor: UIColor? {
        get {
            guard let color = layer.shadowColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    /// MVExtension: Shadow offset of view; also inspectable from Storyboard.
    @IBInspectable var layerShadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }

    /// MVExtension: Shadow opacity of view; also inspectable from Storyboard.
    @IBInspectable var layerShadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    /// MVExtension: Shadow radius of view; also inspectable from Storyboard.
    @IBInspectable var layerShadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    /// MVExtension: Masks to bounds of view; also inspectable from Storyboard.
    @IBInspectable var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
    
    /// MVExtension: Get view's parent view controller
    var parentViewController: UIViewController? {
        weak var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius))

        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
    
}

public extension UIView {
    
    func setShadow(offset: CGSize = CGSize(width: 3, height: 3), color: UIColor, radius: CGFloat = 3, opacity: Float = 1) {
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowColor = color.cgColor
    }
    
    // 该方法通过设置shadowPath避免了离屏渲染导致的动画卡顿
    func setShadowWithoutOffScreenRendering(offset: CGSize = CGSize(width: 3, height: 3), color: UIColor, radius: CGFloat = 3, opacity: Float = 1, shadowPathThickness: CGFloat = 5) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        let path = UIBezierPath()
        let width = bounds.width
        let height = bounds.height
        let x = bounds.origin.x
        let y = bounds.origin.y
        let topLeft = bounds.origin
        let topMiddle = CGPoint(x: x + width / 2, y: y - shadowPathThickness)
        let topRight = CGPoint(x: x + width, y: y)
        let rightMiddle = CGPoint(x: x + width + shadowPathThickness, y: y + height / 2)
        let bottomRight = CGPoint(x: x + width, y: y + height)
        let bottomMiddle = CGPoint(x: x + width / 2, y: y + height + shadowPathThickness)
        let bottomLeft = CGPoint(x: x, y: y + height)
        let leftMiddle = CGPoint(x: x - shadowPathThickness, y: y + height / 2)
        path.move(to: topLeft)
        path.addQuadCurve(to: topRight, controlPoint: topMiddle)
        path.addQuadCurve(to: bottomRight, controlPoint: rightMiddle)
        path.addQuadCurve(to: bottomLeft, controlPoint: bottomMiddle)
        path.addQuadCurve(to: topLeft, controlPoint: leftMiddle)
        layer.shadowPath = path.cgPath
    }
    
    /// 设置指定圆角(该方法会读取bounds，因此必须在layout完成后调用该方法)
    /// - Parameters:
    ///   - corners: 需要设置圆角的位置
    ///   - radius: 圆角大小
    func addCornerRadius(_ corners: [UIRectCorner], radius: CGFloat) {
        var cornerValue: UInt = 0
        corners.forEach { corner in
            cornerValue |= corner.rawValue
        }
        if cornerValue > 0 {
            let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: UIRectCorner(rawValue: cornerValue), cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = bounds
            maskLayer.path = maskPath.cgPath
            layer.mask = maskLayer
        }
    }
    
    func captureScreenImage() -> UIImage? {
        // 参数：截屏区域,是否透明,清晰度
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, UIScreen.main.scale)
        defer {
            UIGraphicsEndImageContext()
        }
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        self.layer.render(in: context)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        return image
    }
    
}

