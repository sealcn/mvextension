//
//  UITableView.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/22.
//

import UIKit

public extension UITableView {

    /// MVExtension: TableView 的最后一个 IndexPath
    @objc var indexPathForLastRow: IndexPath? {
        guard let lastSection = lastSection else { return nil }
        return indexPathForLastRow(inSection: lastSection)
    }

    /// MVExtension: TableView 的最后一个 Section
    var lastSection: Int? {
        return numberOfSections > 0 ? numberOfSections - 1 : nil
    }
}

public extension UITableView {
    
    /// MVExtension: TableView 某个 section 的最后一个 IndexPath
    /// - Parameter section: index
    /// - Returns: IndexPath 或者是空
    @objc func indexPathForLastRow(inSection section: Int) -> IndexPath? {
        guard numberOfSections > 0, section >= 0 else { return nil }
        guard numberOfRows(inSection: section) > 0 else {
            return IndexPath(row: 0, section: section)
        }
        return IndexPath(row: numberOfRows(inSection: section) - 1, section: section)
    }
    
    
    /// 移除 TableView 的 Header
    @objc func removeTableHeaderView() {
        tableHeaderView = nil
    }
    
    
    /// MVExtension: 移除 TableView 的 footer
    @objc func removeTableFooterView() {
        tableFooterView = nil
    }
    
    /// MVExtension: 判断某个 IndexPath 是否在 TableView 中存在
    /// - Parameter indexPath: IndexPath
    /// - Returns: bool
    @objc func isValidIndexPath(_ indexPath: IndexPath) -> Bool {
        return indexPath.section >= 0 &&
            indexPath.row >= 0 &&
            indexPath.section < numberOfSections &&
            indexPath.row < numberOfRows(inSection: indexPath.section)
    }
    
    
    /// MVExtension: 安全的滑动到 TableView 的某个位置
    /// - Parameters:
    ///   - indexPath: indexPath
    ///   - scrollPosition: ScrollPosition
    ///   - animated: animated
    @objc func safeScrollToRow(at indexPath: IndexPath, at scrollPosition: UITableView.ScrollPosition, animated: Bool) {
        guard indexPath.section < numberOfSections else { return }
        guard indexPath.row < numberOfRows(inSection: indexPath.section) else { return }
        scrollToRow(at: indexPath, at: scrollPosition, animated: animated)
    }
    
}
