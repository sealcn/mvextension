//
//  UIFont.Extension.swift
//  MVExtension
//
//  Created by Sunny on 2021/4/13.
//

import UIKit

public extension UIFont {
    var fontSize: CGFloat {
        guard let size = self.fontDescriptor.fontAttributes[.size] as? CGFloat else {
            return self.pointSize
        }
        
        return size
    }
}

public extension UIFont {
    func scale(_ val: CGFloat) -> UIFont {
        return UIFont(name: fontName, size: pointSize * 1.8) ?? self
    }
}
