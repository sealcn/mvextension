//
//  UIApplication.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/15.
//

import UIKit

public extension UIApplication {
    enum Environment {
        case debug
        case testFlight
        case appStore
    }
    
    var isferredEnvironment: Environment {
        #if DEBUG
        return .debug
        #elseif targetEnvironment(simulator)
        return .debug
        #else
        if Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") != nil {
            return .testFlight
        }

        guard let appStoreReceiptUrl = Bundle.main.appStoreReceiptURL else {
            return .debug
        }

        if appStoreReceiptUrl.lastPathComponent.lowercased() == "sandboxreceipt" {
            return .testFlight
        }

        if appStoreReceiptUrl.path.lowercased().contains("simulator") {
            return .debug
        }
        return .appStore
        #endif
    }
    
    /// MVExtension: Application name (if applicable).
    @objc var displayName: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    }

    /// MVExtension: App current build number (if applicable).
    @objc var buildNumber: String? {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
    }

    /// MVExtension: App's current version number (if applicable).
    @objc var version: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}

extension UIApplication {
    public func topVisibleFullScreenWindow() -> UIWindow? {
        let enumerator = self.windows.reversed()
        for window in enumerator {
            let isFullScreen = window.bounds.equalTo(UIScreen.main.bounds)
            let isOnMainScreen = (window.screen == UIScreen.main)
            let isVisible = !window.isHidden && window.alpha > 0
            let isKey = window.isKeyWindow
            if (isFullScreen && isOnMainScreen && isVisible && isKey) {
                return window
            }
        }
        return nil
    }
    
}

public extension UIWindow {
     static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}
