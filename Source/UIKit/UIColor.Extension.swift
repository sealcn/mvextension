//
//  UIColor.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/14.
//

import UIKit

public extension UIColor {
        
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let g = CGFloat((hex & 0x00FF00) >> 8) / 255.0
        let b = CGFloat(hex & 0x0000FF) / 255.0
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    convenience init(hexString str: String, alpha: CGFloat = 1.0) {
        let range = NSMakeRange(0, str.count)
        let hexString = (str as NSString).replacingOccurrences(of: "[^0-9a-fA-F]", with: "", options: .regularExpression, range: range)
        let hex = Int(hexString, radix: 16).require(hint: "Please check the hexString")
        self.init(hex: hex, alpha: alpha)
    }

    @objc static func hex(_ hex: Int) -> UIColor {
        return UIColor.init(hex: hex)
    }
    
    @objc static func hex(_ hex: Int, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor.init(hex: hex, alpha: alpha)
    }
    
    @objc class var random: UIColor {
        get {
            let red = CGFloat(arc4random()%256)/255.0
            let green = CGFloat(arc4random()%256)/255.0
            let blue = CGFloat(arc4random()%256)/255.0
            return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        }
    }
    
    /**
     1.浮点算法：Gray=R0.3+G0.59+B0.11
     2.整数方法：Gray=(R30+G59+B11)/100
     3.移位方法：Gray =(R77+G151+B*28)>>8;
     4.平均值法：Gray=（R+G+B）/3;
     5.仅取绿色：Gray=G;
     */
    @objc var toGray: UIColor {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let gray = (r + g + b) / 3
        return UIColor(red: gray, green: gray, blue: gray, alpha: 1)
    }
}

// MARK: - UInt32Support
public extension UIColor {
    
    /// 计算 UIColor 的明亮值
    ///
    /// - Returns: UIColor 的明亮值
    @objc var bright: CGFloat {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let resR: CGFloat = 0.3 * r// * 255
        let resG: CGFloat = 0.6 * g// * 255
        let resB: CGFloat = 0.1 * b// * 255
        let bright = (resR + resG + resB) * 255
        return bright
    }
    
    @objc var uintOrder32LittleValue: UInt32 {

        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let a1 = (UInt32)(a * 255.0) << 24
        let a2 = (UInt32)(r * a * 255.0) << 16
        let a3 = (UInt32)(g * a * 255.0) << 8
        let a4 = (UInt32)(b * a * 255.0) << 0

        return a1 | a2 | a3 | a4
    }
}
