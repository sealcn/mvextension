//
//  UIViewController.swift
//  MVExtension
//
//  Created by mac on 2021/2/19.
//

import UIKit

public extension UIViewController {
    
    @objc func getTopViewController() -> UIViewController? {
        
        var vc = getTopViewController(vc: UIApplication.shared.windows.first?.rootViewController)
        while vc?.presentedViewController != nil {
            vc = getTopViewController(vc: vc?.presentedViewController)
        }
        return vc
    }

    private func getTopViewController(vc: UIViewController?) -> UIViewController? {
        guard let vc = vc else { return nil }
        if vc.isKind(of: UINavigationController.self) {
            let topVC = (vc as? UINavigationController)?.topViewController
            return getTopViewController(vc: topVC)
        } else if vc.isKind(of: UITabBarController.self) {
            return getTopViewController(vc: (vc as? UITabBarController)?.selectedViewController)
        } else {
            return vc
        }
    }
}
