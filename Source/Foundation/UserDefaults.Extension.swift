//
//  UserDefaults.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/15.
//

import Foundation

public extension UserDefaults {
    
    /// MVExtension: 使用角标的方式操作 UserDefaults
    ///
    /// - Parameter key: 当前 UserDefaults 中的 key
    subscript(key: String) -> Any? {
        get {
            return object(forKey: key)
        }
        set {
            set(newValue, forKey: key)
        }
    }
    
}
