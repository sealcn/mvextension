//
//  DispatchQueue.Extension.swift
//  MVExtension
//
//  Created by Sunny on 2021/4/13.
//

import Foundation
import Dispatch

public extension DispatchQueue {
    @usableFromInline
    internal static var onceTokens = [String]()
    @inlinable
    class func once(file: String = #file, function: String = #function, line: UInt = #line, block: () -> Void) {
        objc_sync_enter(self)
        defer {
            objc_sync_exit(self)
        }
        
        let token = "\(file):\(function):\(line)"
        if onceTokens.contains(token) {
            return
        }
        
        onceTokens.append(token)
        block()
    }
}

public typealias Task = (_ cancel: Bool) -> Void

@discardableResult
public func delay(_ time: TimeInterval, task: @escaping () -> ()) -> Task? {
    
    func dispatch_later(block: @escaping () -> ()) {
        let t = DispatchTime.now() + time
        DispatchQueue.main.asyncAfter(deadline: t, execute: block)
    }
    
    var closure: (() -> Void)? = task
    var result: Task?
    
    let delayedClosure: Task = { cancel in
        if let internalClosure = closure {
            if  cancel == false {
                DispatchQueue.main.async(execute: internalClosure)
            }
        }
        closure = nil
        result = nil
    }
    
    result = delayedClosure
    
    dispatch_later {
        if let delayedClosure = result {
            delayedClosure(false)
        }
    }
    
    return result
}

public func cancel(_ task: Task?) {
    task?(true)
}

public func safeSyncMain(_ closure: () -> Void) {
    if Thread.current.isMainThread {
        closure()
    } else {
        DispatchQueue.main.sync {
            closure()
        }
    }
}
