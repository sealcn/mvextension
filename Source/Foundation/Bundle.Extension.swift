//
//  Bundle.Extension.swift
//  MVExtension
//
//  Created by Sunny on 2021/4/13.
//

import Foundation


extension Bundle {
    
    static func bundle(with name: String) -> Bundle {
        let path = Bundle.main.path(forResource: name, ofType: "bundle").or("")
        return Bundle(path: path).or(Bundle.main)
    }
    
}
