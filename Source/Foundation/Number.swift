//
//  Number.swift
//  MVExtension
//
//  Created by mac on 2021/1/15.
//

import Foundation
import CoreGraphics

public func +(lhs: Int, rhs: Double) -> Double {
    return Double(lhs) + rhs
}

public func +(lhs: Double, rhs: Int) -> Double {
    return lhs + Double(rhs)
}

public func +(lhs: Int, rhs: Float) -> Float {
    return Float(lhs) + rhs
}

public func +(lhs: Float, rhs: Int) -> Float {
    return lhs + Float(rhs)
}

public func +(lhs: Float, rhs: Double) -> Double {
    return Double(lhs) + rhs
}

public func +(lhs: Double, rhs: Float) -> Double {
    return lhs + Double(rhs)
}

public func +(lhs: UInt, rhs: Double) -> Double {
    return Double(lhs) + rhs
}

public func +(lhs: Double, rhs: UInt) -> Double {
    return lhs + Double(rhs)
}

public func +(lhs: UInt, rhs: Float) -> Float {
    return Float(lhs) + rhs
}

public func +(lhs: Float, rhs: UInt) -> Float {
    return lhs + Float(rhs)
}

public func +(lhs: UInt, rhs: Int) -> Int {
    return Int(lhs) + rhs
}

public func +(lhs: Int, rhs: UInt) -> Int {
    return lhs + Int(rhs)
}

public func +(lhs: Int64, rhs: UInt64) -> Int64 {
    return lhs + Int64(rhs)
}

public func +(lhs: UInt64, rhs: Int64) -> Int64 {
    return Int64(lhs) + rhs
}

public func -(lhs: Int, rhs: Double) -> Double {
    return Double(lhs) - rhs
}

public func -(lhs: Double, rhs: Int) -> Double {
    return lhs - Double(rhs)
}

public func -(lhs: Int, rhs: Float) -> Float {
    return Float(lhs) - rhs
}

public func -(lhs: Float, rhs: Int) -> Float {
    return lhs - Float(rhs)
}

public func -(lhs: Float, rhs: Double) -> Double {
    return Double(lhs) - rhs
}

public func -(lhs: Double, rhs: Float) -> Double {
    return lhs - Double(rhs)
}

public func -(lhs: UInt, rhs: Double) -> Double {
    return Double(lhs) - rhs
}

public func -(lhs: Double, rhs: UInt) -> Double {
    return lhs - Double(rhs)
}

public func -(lhs: UInt, rhs: Float) -> Float {
    return Float(lhs) - rhs
}

public func -(lhs: Float, rhs: UInt) -> Float {
    return lhs - Float(rhs)
}

public func -(lhs: UInt, rhs: Int) -> Int {
    return Int(lhs) - rhs
}

public func -(lhs: Int, rhs: UInt) -> Int {
    return lhs - Int(rhs)
}

public func -(lhs: Int64, rhs: UInt64) -> Int64 {
    return lhs - Int64(rhs)
}

public func -(lhs: UInt64, rhs: Int64) -> Int64 {
    return Int64(lhs) - rhs
}

public func *(lhs: Int, rhs: Double) -> Double {
    return Double(lhs) * rhs
}

public func *(lhs: Double, rhs: Int) -> Double {
    return lhs * Double(rhs)
}

public func *(lhs: Int, rhs: Float) -> Float {
    return Float(lhs) * rhs
}

public func *(lhs: Float, rhs: Int) -> Float {
    return lhs * Float(rhs)
}

public func *(lhs: Float, rhs: Double) -> Double {
    return Double(lhs) * rhs
}

public func *(lhs: Double, rhs: Float) -> Double {
    return lhs * Double(rhs)
}

public func *(lhs: UInt, rhs: Double) -> Double {
    return Double(lhs) * rhs
}

public func *(lhs: Double, rhs: UInt) -> Double {
    return lhs * Double(rhs)
}

public func *(lhs: UInt, rhs: Float) -> Float {
    return Float(lhs) * rhs
}

public func *(lhs: Float, rhs: UInt) -> Float {
    return lhs * Float(rhs)
}

public func *(lhs: UInt, rhs: Int) -> Int {
    return Int(lhs) * rhs
}

public func *(lhs: Int, rhs: UInt) -> Int {
    return lhs * Int(rhs)
}

public func *(lhs: Int64, rhs: UInt64) -> Int64 {
    return lhs * Int64(rhs)
}

public func *(lhs: UInt64, rhs: Int64) -> Int64 {
    return Int64(lhs) * rhs
}

public func /(lhs: Int, rhs: Double) -> Double {
    return Double(lhs) / rhs
}

public func /(lhs: Double, rhs: Int) -> Double {
    return lhs / Double(rhs)
}

public func /(lhs: Int, rhs: Float) -> Float {
    return Float(lhs) / rhs
}

public func /(lhs: Float, rhs: Int) -> Float {
    return lhs / Float(rhs)
}

public func /(lhs: Float, rhs: Double) -> Double {
    return Double(lhs) / rhs
}

public func /(lhs: Double, rhs: Float) -> Double {
    return lhs / Double(rhs)
}

public func /(lhs: UInt, rhs: Double) -> Double {
    return Double(lhs) / rhs
}

public func /(lhs: Double, rhs: UInt) -> Double {
    return lhs / Double(rhs)
}

public func /(lhs: UInt, rhs: Float) -> Float {
    return Float(lhs) / rhs
}

public func /(lhs: Float, rhs: UInt) -> Float {
    return lhs / Float(rhs)
}

public func /(lhs: UInt, rhs: Int) -> Int {
    return Int(lhs) / rhs
}

public func /(lhs: Int, rhs: UInt) -> Int {
    return lhs / Int(rhs)
}

public func /(lhs: Int64, rhs: UInt64) -> Int64 {
    return lhs / Int64(rhs)
}

public func /(lhs: UInt64, rhs: Int64) -> Int64 {
    return Int64(lhs) / rhs
}

// MARK: - Core Graphics Calculation
public func +(lhs: CGFloat, rhs: Float) -> CGFloat {
    return lhs + CGFloat(rhs)
}

public func +(lhs: Float, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) + rhs
}

public func +(lhs: CGFloat, rhs: Double) -> CGFloat {
    return lhs + CGFloat(rhs)
}

public func +(lhs: Double, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) + rhs
}

public func +(lhs: CGFloat, rhs: Int) -> CGFloat {
    return lhs + CGFloat(rhs)
}

public func +(lhs: Int, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) + rhs
}

public func +(lhs: CGFloat, rhs: UInt) -> CGFloat {
    return lhs + CGFloat(rhs)
}

public func +(lhs: UInt, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) + rhs
}

public func -(lhs: CGFloat, rhs: Float) -> CGFloat {
    return lhs - CGFloat(rhs)
}

public func -(lhs: Float, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) - rhs
}

public func -(lhs: CGFloat, rhs: Double) -> CGFloat {
    return lhs - CGFloat(rhs)
}

public func -(lhs: Double, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) - rhs
}

public func -(lhs: CGFloat, rhs: Int) -> CGFloat {
    return lhs - CGFloat(rhs)
}

public func -(lhs: Int, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) - rhs
}

public func -(lhs: CGFloat, rhs: UInt) -> CGFloat {
    return lhs - CGFloat(rhs)
}

public func -(lhs: UInt, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) - rhs
}

public func *(lhs: CGFloat, rhs: Float) -> CGFloat {
    return lhs * CGFloat(rhs)
}

public func *(lhs: Float, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

public func *(lhs: CGFloat, rhs: Double) -> CGFloat {
    return lhs * CGFloat(rhs)
}

public func *(lhs: Double, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

public func *(lhs: CGFloat, rhs: Int) -> CGFloat {
    return lhs * CGFloat(rhs)
}

public func *(lhs: Int, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

public func *(lhs: CGFloat, rhs: UInt) -> CGFloat {
    return lhs * CGFloat(rhs)
}

public func *(lhs: UInt, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

public func /(lhs: CGFloat, rhs: Float) -> CGFloat {
    return lhs / CGFloat(rhs)
}

public func /(lhs: Float, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

public func /(lhs: CGFloat, rhs: Double) -> CGFloat {
    return lhs / CGFloat(rhs)
}

public func /(lhs: Double, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

public func /(lhs: CGFloat, rhs: Int) -> CGFloat {
    return lhs / CGFloat(rhs)
}

public func /(lhs: Int, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

public func /(lhs: CGFloat, rhs: UInt) -> CGFloat {
    return lhs / CGFloat(rhs)
}

public func /(lhs: UInt, rhs: CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

public extension String {
    
    var float: Float {
        return Float(self) ?? 0
    }
    
    var int: Int {
        return Int(self) ?? 0
    }
    
    var cgfloat: CGFloat {
        return CGFloat(self.float)
    }
}

public extension Int {
    var cgfloat: CGFloat {
        return CGFloat(self)
    }
}

public extension CGFloat {
    var int: Int {
        return Int(self)
    }
}

public extension UInt8 {
    var int: Int {
        return Int(self)
    }
}

public extension Float {
    var int: Int {
        return Int(self)
        
    }
}

public extension String {
    
    var url: URL? {
        return URL(string: self)
    }
}
