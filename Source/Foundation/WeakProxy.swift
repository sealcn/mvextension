//
//  WeakProxy.swift
//  MVExtension
//
//  Created by Sunny on 2021/4/13.
//

import Foundation

public class WeakProxy: NSObject {
    
    weak var target: NSObject?
    
    public static func proxy(with target: NSObject) -> WeakProxy {
        return WeakProxy.init(target: target)
    }
    
    public convenience init(target: NSObject) {
        self.init()
        self.target = target
    }
    
    public override func forwardingTarget(for aSelector: Selector!) -> Any? {
        return target
    }

}
