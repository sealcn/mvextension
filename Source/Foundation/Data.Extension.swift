//
//  Data.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/15.
//

import Foundation
import CryptoKit
import CommonCrypto

extension Data {
    
    private enum CryptoAlgorithm {
        case md5, sha1, sha224, sha256, sha384, sha512
        
        var digestLength: Int {
            var result: Int32 = 0
            switch self {
            case .md5: result = CC_MD5_DIGEST_LENGTH
            case .sha1: result = CC_SHA1_DIGEST_LENGTH
            case .sha224: result = CC_SHA224_DIGEST_LENGTH
            case .sha256: result = CC_SHA256_DIGEST_LENGTH
            case .sha384: result = CC_SHA384_DIGEST_LENGTH
            case .sha512:  result = CC_SHA512_DIGEST_LENGTH
            }
            return Int(result)
        }
    }
    
    
    /// MVExtension: MD5
    private var p_md5: String {
        if #available(iOS 13.0, *) {
            let digest = Insecure.MD5.hash(data: self)
            return digest.map {
                String.init(format: "%02hhx", $0)
            }.joined()
        } else {
            var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            _ = self.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) in
                return CC_MD5(bytes.baseAddress, CC_LONG(self.count), &digest)
            }
            return digest.reduce(into: "") { $0 += String(format: "%02x", $1) }
        }
    }
    
    
    var md5:    String { return p_md5 }
    var sha1:   String { return digest(use: .sha1) }
    var sha224: String { return digest(use: .sha224) }
    var sha256: String { return digest(use: .sha256) }
    var sha384: String { return digest(use: .sha384) }
    var sha512: String { return digest(use: .sha512) }
    
   
    private func digest(use algorithm: CryptoAlgorithm) -> String {
        
        var digest: [CUnsignedChar]
        let lengh = algorithm.digestLength
        digest = Array(repeating: 0, count: lengh)
        
        _ = self.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> UnsafeMutablePointer<UInt8>? in
            switch algorithm {
            case .md5: fatalError("use new method instead.")
            case .sha1: return CC_SHA1(bytes.baseAddress, CC_LONG(self.count), &digest)
            case .sha224: return CC_SHA224(bytes.baseAddress, CC_LONG(self.count), &digest)
            case .sha256: return CC_SHA256(bytes.baseAddress, CC_LONG(self.count), &digest)
            case .sha384: return CC_SHA384(bytes.baseAddress, CC_LONG(self.count), &digest)
            case .sha512: return CC_SHA512(bytes.baseAddress, CC_LONG(self.count), &digest)
            }
        }
        return digest.reduce("") { $0 + String(format: "%02hhx", $1)}
    }
}
