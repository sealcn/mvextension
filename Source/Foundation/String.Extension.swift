//
//  String.Localized.swift
//  MVExtension
//
//  Created by mac on 2021/1/14.
//

import Foundation

// MARK: localized
public extension String {
    
    var localized: String {
         return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func localized(with args: StringLiteralType...) -> String {
        var localizedString = self.localized
        args.forEach({
            localizedString = localizedString.replacingOccurrences(of: "%s", with: $0)
        })
        return localizedString
    }
    
    func localized(with map: [String: String]) -> String {
        var localizedString = self.localized
        map.forEach({
            localizedString = localizedString.replacingOccurrences(of: $0, with: $1)
        })
        return localizedString
    }
}

@objc public extension NSString {
    
    @objc func localized() -> NSString {
        let s = self as String
        let l = s.localized
        let nsl = l as NSString
        return nsl
    }

    @objc func localized(args: [String] = []) -> NSString {
        var l = self.localized()
        args.forEach({
            l = l.replacingOccurrences(of: "%s", with: $0) as NSString
        })
        return l
    }

    @objc func localized(map: [String: String]) -> NSString {
        let s = self as String
        let l = s.localized(with: map)
        let nsl = l as NSString
        return nsl
    }
}


// MARK: Encrypt
public extension String {
    
    var base64: String {
        let utf8str:Data? = self.data(using: String.Encoding.utf8)
        guard let utf8Data = utf8str else{
            return ""
        }
        
        let base64Encoded:String = utf8Data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        return base64Encoded
        
    }
    
    private var data: Data? {
        return self.data(using: .utf8)
    }
    
    var md5: String {
        return data?.md5 ?? self
    }
    
    var sha: String {
        return data?.sha1 ?? self
    }
    
    var sha224: String {
        return data?.sha224 ?? self
    }
    
    var sha256: String {
        return data?.sha256 ?? self
    }
    
    var sha384: String {
        return data?.sha384 ?? self
    }
    
    var sha512: String {
        return data?.sha512 ?? self
    }
    
}

// MARK: URLEncode
public extension String {
    
    func urlEncoded() -> String {
        let encodeUrlString = self.addingPercentEncoding(withAllowedCharacters:
            .urlQueryAllowed)
        return encodeUrlString ?? ""
    }
    
    //将编码后的url转换回原始的url
    func urlDecoded() -> String {
        return self.removingPercentEncoding ?? ""
    }
}

@objc public extension NSString {

    @objc func urlEncoded() -> NSString {
        let l = self as String
        let  dl = l.urlEncoded()
        return dl as NSString
    }

    //将编码后的url转换回原始的url
    @objc func urlDecoded() -> NSString {
        let l = self as String
        let  dl = l.urlDecoded()
        return dl as NSString
    }
}


// MARK: subscript
public extension String {
    subscript(index: Int) -> String {
        get {
            return String(self[self.index(self.startIndex, offsetBy: index)])
        }
        set {
            let tmp = self
            self = ""
            for (idx, item) in tmp.enumerated() {
                if idx == index {
                    self += "\(newValue)"
                } else {
                    self += "\(item)"
                }
            }
        }
    }
}

