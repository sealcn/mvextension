//
//  Date.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/15.
//

import Foundation
//
//public extension Date {
//
//    enum DayNameStyle {
//        case threeLetters
//        case oneLetter
//        case full
//    }
//
//
//    enum MonthNameStyle {
//        case threeLetters
//        case oneLetter
//        case full
//    }
//
//}
//
//
//public extension Date {
//
//    /// MVExtension: 用户当前的 Calender
//    var calender: Calendar {
//        return Calendar(identifier: Calendar.current.identifier)
//    }
//
//    /// MVExtension: 季节.
//    ///
//    ///        Date().quarter -> 3 // date in third quarter of the year.
//    ///
//    var quarter: Int {
//        let month = Double(calender.component(.month, from: self))
//        let numberOfMonths = Double(calender.monthSymbols.count)
//        let numberOfMonthInQuarter = numberOfMonths / 4
//        return Int(ceil(month / numberOfMonthInQuarter))
//    }
//
//    var weekOfYear: Int {
//        return calender.component(.weekOfYear, from: self)
//    }
//
//    var weekOfMonth: Int {
//        return calender.component(.weekOfMonth, from: self)
//    }
//
//    var year: Int {
//        get {
//            return calender.component(.year, from: self)
//        }
//        set {
//            guard newValue > 0 else { return }
//            let currentYear = calender.component(.year, from: self)
//            let yearToAdd = newValue - currentYear
//            if let date = calender.date(byAdding: .year, value: yearToAdd, to: self) {
//                self = date
//            }
//        }
//    }
//
//    var month: Int {
//        get {
//            return calender.component(.month, from: self)
//        }
//        set {
//            guard let allowedRange = calender.range(of: .month, in: .year, for: self),
//                  allowedRange.contains(newValue) else { return }
//
//            let currentMonth = calender.component(.month, from: self)
//            let monthsToAdd = newValue - currentMonth
//            if let date = calender.date(byAdding: .month, value: monthsToAdd, to: self) {
//                self = date
//            }
//        }
//    }
//
//    var day: Int {
//        get {
//            return calender.component(.day, from: self)
//        }
//        set {
//            guard let allowedRange = calender.range(of: .day, in: .month, for: self),
//                  allowedRange.contains(newValue) else { return }
//
//            let currentDay = calender.component(.day, from: self)
//            let dayToAdd = newValue - currentDay
//            if let date = calender.date(byAdding: .day, value: dayToAdd, to: self) {
//                self = date
//            }
//        }
//    }
//
//    var weekDay: Int {
//        return calender.component(.weekday, from: self)
//    }
//
//}

public extension Date {
    
    /// MVExtension: 通过Unix时间戳创建 Date
    init?(unixTimestamp: Double) {
        self.init(timeIntervalSince1970: unixTimestamp)
    }
    
}
