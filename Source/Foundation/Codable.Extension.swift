//
//  Codable.Extension.swift
//  MVExtension
//
//  Created by Sunny on 2021/4/13.
//

import Foundation

public extension Encodable {
    func encode() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func encodeToJSON() -> String {
        guard let data = try? encode() else {
            return ""
        }
        guard let str = String(data: data, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/") else {
            return ""
        }
        return str
    }
}

public extension Data {
    func decode<T: Decodable>(type: T.Type = T.self) throws -> T {
        do {
            let res = try JSONDecoder().decode(type, from: self)
            return res
        } catch {
            print(error)
            throw error
        }
    }

}
