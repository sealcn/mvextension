//
//  NSError.Extension.swift
//  MVExtension
//
//  Created by Sunny on 2021/4/13.
//

import Foundation

extension NSError {
    
    public convenience init(domain: String, code: Int, localized description: String? = nil) {
        if let desc = description {
            self.init(domain: domain, code: code, userInfo: [NSLocalizedDescriptionKey : desc])
        } else {
            self.init(domain: domain, code: code, userInfo: nil)
        }
    }
}
