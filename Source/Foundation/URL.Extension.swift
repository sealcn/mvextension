//
//  NSURL.Extension.swift
//  MVExtension
//
//  Created by mac on 2021/1/15.
//

import Foundation

// 加密
public extension URL {
    
    /// MVExtension: 对本地文件就行 md5 加密
    var md5: String {
        guard let data = try? Data(contentsOf: self) else {
            return ""
        }
        return data.md5
    }
}

extension URL: ExpressibleByStringLiteral {
    
    // By using 'StaticString' we disable string interpolation, for safety
    // let url: URL = "https://www.baidu.com"
    // let task = URLSession.shared.dataTask(with: url)
    // let task2 = URLSession.shared.dataTask(with: "https://www.baidu.com")
    
    public init(stringLiteral value: StaticString) {
        self = URL(string: "\(value)").require(hint: "Invalid URL string literal: \(value)")
    }
    
}
