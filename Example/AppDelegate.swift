//
//  AppDelegate.swift
//  Example
//
//  Created by mac on 2021/1/14.
//

import UIKit
import MVExtension

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        

        // MARK: String
        stringMethods()

        //MARK: URL
        let url: URL = "http://www.baidu.com"

        func someMethod(url: URL) {
            print(url.absoluteString)
        }

        someMethod(url: "http://www.baidu.com")



        //MARK: Optional
        var value: String?
//        print(value.require(hint: "value cannot be nil")) // 抛出异常
        print(value.or("default value"))
        if value.hasSome {
            print("value is not nil")
        }

        value.ifSome({
            print($0)
        })

        value = "value"
        print(value.require(hint: "value cannot be nil"))
        print(value.or("default value"))
        if value.hasSome {
            print("value is not nil")
        }

        value.ifSome({
            print($0)
        })

        //MARK: UIColor
        let _ = UIColor(hex: 0x111111)
        let _ = UIColor(hex: 0x111111, alpha: 0.5)
        let _ = UIColor.init(hexString: "0xFFFFFF")
        let _ = UIColor.init(hexString: "0xFFFFFF", alpha: 0.5)
        let _ = UIColor.random
        let _ = UIColor.red.toGray
        let _ = UIColor.red.bright
        let _ = UIColor.red.uintOrder32LittleValue



//        print(Date().era)


        UserDefaults.standard["key"] = "value"
        print(UserDefaults.standard["key"])

        
        let date = Date.init(unixTimestamp: 10101010101)

        return true
        
        
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate {
    func stringMethods() {
        print("something".localized)
                
        print("something * %s".localized(with: "5"))
        print("something * %1s * %2s".localized(with: ["%1s": "100", "%2s": "200"]))

        print("something".md5)
        print("something".base64)
        print("something".sha)
        print("something".sha224)
        print("something".sha256)
        print("something".sha384)
        print("something".sha512)
        

        for i in 0...8 {
            print("something"[i])
        }

        let url = "http://www.baidu.com/你好"
        let encodeUrl = url.urlEncoded()
        let decodeUrl = encodeUrl.urlDecoded()

        print(encodeUrl)
        print(decodeUrl)
    }
}

